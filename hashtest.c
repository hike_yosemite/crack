#include <stdio.h>
#include "md5.h"
#include <string.h>

int main()
{
    char word[100];
    printf("Enter a word: ");
    scanf("%s", word);
    
    char *hash = md5(word, strlen(word));
    
    printf("%s\n", hash);
}